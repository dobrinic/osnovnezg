<?php

require_once 'php_action/db_connect.php';

if($_GET['id']) {
	$id = $_GET['id'];

	$sql = "SELECT * FROM skola WHERE skolaID = {$id}";
	$result = $connection->query($sql) or die($connection->error);

	$data = $result->fetch_assoc();

	$connection->close();

?>

<!DOCTYPE html>
<html lang="hr">
    <head>
        <meta charset="utf-8">
    	<title>O školi</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<style type="text/css">
    	</style>
    </head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-default">
                        <div class="panel-heading">
							<h2>O školi: <small><?php echo $data['nazivSkole'] ?></small></h2>
						</div>
						<div class="panel-body">
							<form action="php_action/update.php" method="post">
								<div class="form-group">
								    <label for="nazivSkole">Naziv škole</label>
								    <input type="text" class="form-control" id="nazivSkole" readonly name="nazivSkole" value="<?php echo $data['nazivSkole'] ?>">
								</div>
								<div class="form-group">
								    <label for="posebnost">Posebnost</label>
								    <input type="text" class="form-control" id="posebnost" readonly name="posebnost" value="<?php echo $data['posebnost'] ?>">
								</div>
								<div class="form-group">
									<label for="adresa">Adresa</label>
									<input type="text" class="form-control" id="adresa" readonly name="adresa" value="<?php echo $data['adresa'] ?>">
								</div>
								<div class="form-group">
									<label for="cetvrt">Četvrt</label>
									<input type="text" class="form-control" id="cetvrt" readonly name="cetvrt" value="<?php echo $data['cetvrt'] ?>">
								</div>
								<div class="form-group">
									<label for="vrstaSkole">Vrsta škole</label>
									<input type="text" class="form-control" id="vrstaSkole" readonly name="vrstaSkole" value="<?php echo $data['nazivVrste'] ?>">
								</div>
								<div class="form-group">
									<label for="koordE">Koordinata (E)</label>
									<input type="text" class="form-control" id="koordE" readonly name="koordE" value="<?php echo $data['koordE'] ?>">
								</div>
								<div class="form-group">
									<label for="koordN">Koordinata (N)</label>
									<input type="text" class="form-control" id="koordN" readonly name="koordN" value="<?php echo $data['koordN'] ?>">
								</div>
								<a href="index.php" type="button" class="btn btn-warning">Nazad</a>
								<a href='edit.php?id=<?php echo $data['skolaID'] ?>' type='button' class="btn btn-primary pull-right">Uredi</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>

<?php
}
?>
