<!DOCTYPE html>
<html lang="hr">
    <head>
        <meta charset="utf-8">
    	<title>Dodaj školu</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<style type="text/css">
    	</style>
    </head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-default">
                        <div class="panel-heading">
							<h1>Dodaj školu</h1>
						</div>
						<div class="panel-body">
							<form action="php_action/create.php" method="post">
								<div class="form-group">
								    <label for="nazivSkole">Naziv škole</label>
								    <input type="text" class="form-control" id="nazivSkole" required name="nazivSkole" placeholder="Naziv škole">
								</div>
								<div class="form-group">
								    <label for="posebnost">Posebnost</label>
								    <input type="text" class="form-control" id="posebnost" name="posebnost" placeholder="Posebnost">
								</div>
								<div class="form-group">
									<label for="adresa">Adresa</label>
									<input type="text" class="form-control" id="adresa" required name="adresa" placeholder="Adresa">
								</div>
								<div class="form-group">
									<label for="cetvrt">Četvrt</label>
									<input type="text" class="form-control" id="cetvrt" required name="cetvrt" placeholder="Četvrt">
								</div>
								<div class="form-group">
									<label for="vrstaSkole">Vrsta škole</label>
									<input type="text" class="form-control" id="vrstaSkole" required name="vrstaSkole" placeholder="Vrsta škole">
								</div>
								<div class="form-group">
									<label for="koordE">Koordinata (E)</label>
									<input type="text" class="form-control" id="koordE" required name="koordE" placeholder="Koordinata (E)">
								</div>
								<div class="form-group">
									<label for="koordN">Koordinata (N)</label>
									<input type="text" class="form-control" id="koordN" required name="koordN" placeholder="Koordinata (N)">
								</div>
								<a href="index.php" type="button" class="btn btn-warning">Nazad</a>
                                <button type="submit" name="submit" class="btn btn-primary pull-right">Upiši</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>
