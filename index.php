<?php

require_once 'php_action/db_connect.php';
// izvor - https://www.w3schools.com/php/php_sessions.asp
session_start();

?>

<!DOCTYPE html>
<html lang="hr">
    <head>
        <meta charset="utf-8">
    	<title>Osnovne škole</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<style type="text/css">
        #posebnost-form, #posebnost-form .form-group, #posebnost-form button{
            display: inline-block;
        }
    	</style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-12">
                    <?php require_once 'php_action/messages.php'; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="text-center">Osnovne Škole</h1>
                            <a href="create.php" class="btn btn-primary btn-lg" role="button">Dodaj novu školu</a>
                            <a href="reports.php" class="btn btn-primary btn-lg" role="button">Izvještaji</a>
                            <div id="posebnost-form" class="pull-right">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                                    <div class="form-group" >
                                        <select class="form-control" id="posebnost-select" name="posebnost">
                                            <option hidden >Odaberite posebnost:</option>
                                            <option value="sveSkole" >Sve škole</option>
                                            <?php
                                                // učitajmo grupirano sve posebnosti poredane od one sa najviše pojavljivanja
                                                $sql = "SELECT posebnost , count(*) as count FROM skola WHERE posebnost != '' GROUP BY posebnost ORDER BY count DESC;";
                                                $result = $connection->query($sql) or die('Could not connect: ' . mysqli_error());
                                                //ako prethodan upit nije vratio ni jedan red spojimo datoteku za učitavanje i pozovimo metodu za učitavanje
                                                if (!mysqli_affected_rows($connection)) {
                                                    require_once 'php_action/read-file.php';
                                                    readInCSV($connection);
                                                    //sad kad je baza popunjena ponovimo upit
                                                    $result = $connection->query($sql) or die('Could not connect: ' . mysqli_error());
                                                }
                                                if($result->num_rows > 0) {
                                    				while($row = $result->fetch_array()) {
                                                         echo '<option value="'.$row['posebnost'].'">'.$row['posebnost'].'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <button type="submit" name="submit-posebnosti" class="btn btn-primary">Odaberi</button>
                                </form>
                            </div>
                        </div>

                        <table class="table table-hover">
                            <tr>
                                <th>R.B.</th>
                                <th>Naziv škole</th>
                                <th>Posebnost</th>
                                <th>Adresa</th>
                                <th>Četvrt</th>
                                <th>Vrsta škole</th>
                                <th>Koordinate</th>
                                <th>Akcija</th>
                            </tr>

                        <?php

                        if(isset($_POST['submit-posebnosti']) && $_POST['posebnost'] !== 'sveSkole') {
                            $selected_val = $_POST['posebnost'];
                            $sql = "SELECT * FROM skola WHERE posebnost like '%$selected_val%';";
                            $result = $connection->query($sql) or die($connection->error);
                            $connection->close();
                        }else{
                            $sql = "SELECT * FROM skola WHERE nazivSkole != ''";
                            $result = $connection->query($sql) or die('Could not connect: ' . mysqli_error());
                            $rowNum = mysqli_affected_rows($connection);
                            $_SESSION["rowNum"] = $rowNum;
                            $connection->close();
                        }

                        if($result->num_rows > 0) {
                            $i = 1;
            				while($row = $result->fetch_assoc()) {
            					echo
                            "<tr>
                                <td>".$i."</td>
            					<td>".$row['nazivSkole']."</td>
            					<td>".$row['posebnost']."</td>
            					<td>".$row['adresa']."</td>
            					<td>".$row['cetvrt']."</td>
            					<td>".$row['nazivVrste']."</td>
            					<td>".$row['koordE'].", ".$row['koordN']."</td>
            					<td width='200'>
            						<a href='show.php?id=".$row['skolaID']."'><button type='button' class='btn btn-primary btn-sm' >Prikaži</button></a>
            						<a href='edit.php?id=".$row['skolaID']."'><button type='button' class='btn btn-success btn-sm' >Uredi</button></a>
            						<a href='remove.php?id=".$row['skolaID']."'><button type='button' class='btn btn-danger btn-sm' >Obriši</button></a>
            					</td>
            				</tr>";
                            $i++;
                            }
                        }
                        ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
