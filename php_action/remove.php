<?php

require_once 'db_connect.php';

if(isset($_POST['submit'])) {
	$id = $_POST['id'];

	$sql = "DELETE FROM skola WHERE skolaID = {$id}";
	if($connection->query($sql) === TRUE) {
		header("Location: ../index.php?success=Uspješno ste obrisali školu!");
	} else {
		header('Location: ../index.php?error=Erorr while updating record : ' . $connection->error . '');
	}
	$connection->close();
}

?>
