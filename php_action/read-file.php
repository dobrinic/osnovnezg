<?php

function readInCSV($connection){
	if (($handle = fopen("data/osnovnezg.csv", "r")) === FALSE ) {
	        echo "readJobsFromFile: Failed to open file.";
	        die;
	    }else{
			$i = 0;
			while (!feof($handle)){
				$data = fgetcsv($handle, 1000, ",");

				$nazivSkole = formatWords($data[0], ['škola', 'osnovna', 'glazbena', 'odgoj', 'obrazovanje']);
				$posebnost = empty($data[1]) ? NULL : $data[1];
				$adresa = formatWords($data[2], ['ulica', 'trg']);
				$cetvrt = formatWords($data[5], ['grad']);
				$koordN = $data[7];
				$koordE = $data[6];
				$nazivVrste = $data[3];

				if ($i > 0) {
					$sql = "INSERT INTO skola (nazivSkole, posebnost, adresa, cetvrt, koordN, koordE, nazivVrste)
					VALUES ('$nazivSkole', '$posebnost', '$adresa', '$cetvrt', '$koordN', '$koordE', '$nazivVrste');";
					if($connection->query($sql) === FALSE) {
						echo "Error updating record : " . $connection->error;
					}
				}
			    $i++;
			}
		fclose($handle);
	}
}

function formatWords($value, $excludedWords){
	$value = mb_convert_encoding($value, "UTF-8");
	$value = mb_convert_case ($value, MB_CASE_LOWER);
	$value = str_replace(" -", "-", $value);
	$valuesArray = explode(' ', $value);
	$value = '';

	foreach ($valuesArray as $index => $word) {
		// regex ako string počinje i završava navodnoikom
		if (preg_match('/^(["\']).*\1$/m', $word)) {
			$word = trim($word, '"');
			$word = checkformatConditions($word, $index, $excludedWords);
			$word = '"'.$word.'"';
		}// ako se navodnik nalazi na početku stringa (znamo da nije na kraju jer je prošao prvi if)
		elseif (mb_strpos($word, '"') === 0) {
			$word = trim($word, '"');
			$word = checkformatConditions($word, $index, $excludedWords);
			$word = '"'.$word;
		}// izvuci stringove koji sadrže povlaku
		else if (mb_strpos($word, '-')) {
			$word = trim($word, '-');
			unset($valuesArray[$index]);
			$hifenArray = explode('-', $word);
			foreach ($hifenArray as $val) {
				// $val = trim($val, '-');
				$val = checkformatConditions($val, 1, $excludedWords);
				$wordArray[] = $val;
			}
			$word = implode('-', $wordArray);
		}else{
			$word = checkformatConditions($word, $index, $excludedWords);
		}
		$value .= $word . ' ';
	}
	return trim($value);
}

function checkformatConditions($word, $index, $excludedWords){
	// ako je riječ u popisu riječi koje moraju početi malim slovom a nije na prvom mjestu vrati ju nepromjenjenu
	if (($index > 0 && strlen($word) < 3) || (in_array($word, $excludedWords) && $index > 0 )) {
		return $word;
	} else {
		return mb_convert_case($word, MB_CASE_TITLE, "UTF-8");
	}
}

?>
