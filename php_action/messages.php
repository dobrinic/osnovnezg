<?php

// Izvor - https://stackoverflow.com/questions/11988061/php-redirect-to-page-with-message
if(!empty($_GET['success'])) {
	$message = $_GET['success'];
	echo
	'<div class="alert alert-success alert-dismissible">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>'.$message.'</strong>
	</div>';
}elseif (!empty($_GET['error'])) {
	$message = $_GET['error'];
	echo
	'<div class="alert alert-danger  alert-dismissible">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>'.$message.'</strong>
	</div>';
}

?>
