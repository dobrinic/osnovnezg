<?php

require_once 'php_action/db_connect.php';

if($_GET['id']) {
	$id = $_GET['id'];

	$sql = "SELECT * FROM skola WHERE skolaID = {$id}";
	$result = $connection->query($sql);
	$data = $result->fetch_assoc();

	$connection->close();
?>

<!DOCTYPE html>
<html lang="hr">
    <head>
        <meta charset="utf-8">
    	<title>Obriši školu</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<style type="text/css">
    	</style>
    </head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-danger">
                        <div class="panel-heading">
							<h3>Želite li zaista obrisati školu:</h3>
							<h4><?php echo $data['nazivSkole'] ?></h4>
						</div>
						<div class="panel-body">
							<form action="php_action/remove.php" method="post">
								<input type="hidden" name="id" value="<?php echo $data['skolaID']?>" />
								<a href="index.php" type="button" class="btn btn-warning">Nazad</a>
								<button type="submit" name="submit" class="btn btn-primary pull-right">Potvrdi</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>

<?php
}
?>
