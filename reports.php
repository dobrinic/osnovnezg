<?php

require_once 'php_action/db_connect.php';
session_start();
$rowNum = $_SESSION["rowNum"];

?>

<!DOCTYPE html>
<html lang="hr">
    <head>
        <meta charset="utf-8">
    	<title>Izvještaji</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<style type="text/css">
        .panel-heading h1{
            width: 80%;
            display: inline-block;
        }
    	</style>
    </head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="index.php" type="button" class="btn btn-warning">Nazad</a>
							<h1 class="text-center">Izvještaji</h1>
						</div>
						<div class="panel-body">
                            <div class="col-md-6">
        						<h2 class="text-center">Škole po četvrtima</h2>
    							<table class="table table-hover">
                                    <tr>
                                        <th>R.B.</th>
                                        <th>Četvrt</th>
                                        <th>Broj škola</th>
                                        <th>Postotak</th>
                                    </tr>

                                    <?php

                                    $sql = "SELECT cetvrt , count(*) as count FROM skola WHERE nazivSkole != '' GROUP BY cetvrt ORDER BY count DESC;";
                                    $result = $connection->query($sql) or die($connection->error);

                                    if($result->num_rows > 0) {
                                        $i = 1;
                        				while($row = $result->fetch_assoc()) {
                        					echo
                                        "<tr>
                                            <td>".$i."</td>
                        					<td>".$row['cetvrt']."</td>
                        					<td>".$row['count']."</td>";
                                            ?>
                        					<td><?php echo number_format($row['count']/$rowNum*100, 2) ?> %</td>
                        				</tr>
                                        <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h2 class="text-center">Škole po vrsti ustanove</h2>
    							<table class="table table-hover">
                                    <tr>
                                        <th>R.B.</th>
                                        <th>Četvrt</th>
                                        <th>Broj škola</th>
                                        <th>Postotak</th>
                                    </tr>

                                    <?php

                                    $sql = "SELECT nazivVrste , count(*) as count FROM skola WHERE nazivSkole != '' GROUP BY nazivVrste ORDER BY count DESC;";
                                    $result = $connection->query($sql) or die($connection->error);
                                    $connection->close();

                                    if($result->num_rows > 0) {
                                        $i = 1;
                        				while($row = $result->fetch_assoc()) {
                        					echo
                                        "<tr>
                                            <td>".$i."</td>
                        					<td>".$row['nazivVrste']."</td>
                        					<td>".$row['count']."</td>";
                                            ?>
                        					<td><?php echo number_format($row['count']/$rowNum*100, 2) ?> %</td>
                        				</tr>
                                        <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>
